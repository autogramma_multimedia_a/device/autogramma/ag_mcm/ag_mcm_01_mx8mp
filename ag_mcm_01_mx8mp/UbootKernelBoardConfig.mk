# from BoardConfig.mk
TARGET_BOOTLOADER_POSTFIX := bin
UBOOT_POST_PROCESS := true

TARGET_BOOTLOADER_CONFIG := \
	imx8mp-var-dart:imx8mp_ag_mcm_01_defconfig
#	imx8mp-var-dart-uuu:imx8mp_var_dart_android_uuu_defconfig

ifeq ($(IMX8MP_USES_GKI),true)
TARGET_KERNEL_DEFCONFIG:=ag_mcm_01_mx8mp_defconfig
TARGET_KERNEL_GKI_DEFCONF:= ag_mcm_01_mx8mp.fragment

else
TARGET_KERNEL_DEFCONFIG := ag_mcm_01_mx8mp_defconfig
endif

TARGET_KERNEL_ADDITION_DEFCONF := android_addition_defconfig


# absolute path is used, not the same as relative path used in AOSP make
TARGET_DEVICE_DIR := $(patsubst %/, %, $(dir $(realpath $(lastword $(MAKEFILE_LIST)))))

# define bootloader rollback index
BOOTLOADER_RBINDEX ?= 0

